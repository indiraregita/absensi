<!DOCTYPE html>
<html>

<head>
    <title>Masuk!</title>

    <!-- Change your font in here -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">

    <!-- add icon link -->
    <link rel="icon" href="images/tittle.png" type="image/x-icon">

    <!-- add icon link -->
    <link rel="stylesheet" type="text/css" href="css/style-login.css">

    <!-- Font Google -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Courgette&display=swap" rel="stylesheet">

</head>
<script src="https://kit.fontawesome.com/d4b981459a.js" crossorigin="anonymous"></script>

<body>
    <div class="form-signin">
        <div class="name">
            <span class="main-title text-uupercase text-center">MyPresence</span>
        </div>
        <div class="title">
            <span class="main-title text-uppercase text-center">Login</span>
            <p class="subtitle text-center">
            </p>
        </div>

        <div class="form-group">
            <input name="nis" class="form-control" placeholder="NIS" value="" autofocus="autofocus" type="text"
                id="UserNis" required="required">
        </div>
        <div class="form-group">
            <input name="password" class="form-control" placeholder="Password" value="" type="password"
                id="UserPassword" required="required">
        </div>
        <div class="form-group subform">
            <label class="checkbox-inline" for="UserRememberMe">
                <input type="checkbox" name="rememberMe" value="1" id="UserRememberMe">Remember me
            </label>
        </div>
        <div class="form-group">
            <button class="btn-signin">
                Masuk
            </button>
        </div>
    </div>

</body>

</html>